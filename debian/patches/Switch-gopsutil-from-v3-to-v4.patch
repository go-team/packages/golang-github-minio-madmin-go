From: Guillem Jover <gjover@sipwise.com>
Date: Tue, Oct 1 23:50:18 2024 +0200
Subject: Switch gopsutil from v3 to v4

We need to update the gopsutil API usage from v3 to v4, so that we can
update the goputil version in Debian. This involves removing the hardcoded
v3 suffix, and updating the usage of sensors from hosts to the new sensors
module.

---

diff --git a/health-old.go b/health-old.go
index 82181e8..d57256c 100644
--- a/health-old.go
+++ b/health-old.go
@@ -24,11 +24,12 @@ import (
 	"math/big"
 	"time"
 
-	"github.com/shirou/gopsutil/v3/cpu"
-	diskhw "github.com/shirou/gopsutil/v3/disk"
-	"github.com/shirou/gopsutil/v3/host"
-	"github.com/shirou/gopsutil/v3/mem"
-	"github.com/shirou/gopsutil/v3/process"
+	"github.com/shirou/gopsutil/cpu"
+	diskhw "github.com/shirou/gopsutil/disk"
+	"github.com/shirou/gopsutil/host"
+	"github.com/shirou/gopsutil/mem"
+	"github.com/shirou/gopsutil/process"
+	"github.com/shirou/gopsutil/sensors"
 )
 
 // HealthInfoV0 - MinIO cluster's health Info version 0
@@ -228,7 +229,7 @@ type ServerMemInfo struct {
 type ServerOsInfo struct {
 	Addr    string                 `json:"addr"`
 	Info    *host.InfoStat         `json:"info,omitempty"`
-	Sensors []host.TemperatureStat `json:"sensors,omitempty"`
+	Sensors []sensors.TemperatureStat `json:"sensors,omitempty"`
 	Users   []host.UserStat        `json:"users,omitempty"`
 	Error   string                 `json:"error,omitempty"`
 }
diff --git a/health.go b/health.go
index 795debe..3e793da 100644
--- a/health.go
+++ b/health.go
@@ -37,14 +37,15 @@ import (
 	"syscall"
 	"time"
 
-	"github.com/minio/madmin-go/v3/cgroup"
-	"github.com/minio/madmin-go/v3/kernel"
+	"github.com/minio/madmin-go/cgroup"
+	"github.com/minio/madmin-go/kernel"
 	"github.com/prometheus/procfs"
-	"github.com/shirou/gopsutil/v3/cpu"
-	"github.com/shirou/gopsutil/v3/disk"
-	"github.com/shirou/gopsutil/v3/host"
-	"github.com/shirou/gopsutil/v3/mem"
-	"github.com/shirou/gopsutil/v3/process"
+	"github.com/shirou/gopsutil/cpu"
+	"github.com/shirou/gopsutil/disk"
+	"github.com/shirou/gopsutil/host"
+	"github.com/shirou/gopsutil/mem"
+	"github.com/shirou/gopsutil/process"
+	"github.com/shirou/gopsutil/sensors"
 )
 
 // NodeCommon - Common fields across most node-specific health structs
@@ -403,7 +404,7 @@ type OSInfo struct {
 	NodeCommon
 
 	Info    host.InfoStat          `json:"info,omitempty"`
-	Sensors []host.TemperatureStat `json:"sensors,omitempty"`
+	Sensors []sensors.TemperatureStat `json:"sensors,omitempty"`
 }
 
 // TimeInfo contains current time with timezone, and
@@ -463,7 +464,7 @@ func GetOSInfo(ctx context.Context, addr string) OSInfo {
 	}
 	osInfo.Info.KernelVersion = kr
 
-	osInfo.Sensors, _ = host.SensorsTemperaturesWithContext(ctx)
+	osInfo.Sensors, _ = sensors.TemperaturesWithContext(ctx)
 
 	return osInfo
 }
@@ -789,7 +790,7 @@ type ProcInfo struct {
 	CreateTime     int64                      `json:"create_time,omitempty"`
 	CWD            string                     `json:"cwd,omitempty"`
 	ExecPath       string                     `json:"exec_path,omitempty"`
-	GIDs           []int32                    `json:"gids,omitempty"`
+	GIDs           []uint32                   `json:"gids,omitempty"`
 	IOCounters     process.IOCountersStat     `json:"iocounters,omitempty"`
 	IsRunning      bool                       `json:"is_running,omitempty"`
 	MemInfo        process.MemoryInfoStat     `json:"mem_info,omitempty"`
@@ -805,7 +806,7 @@ type ProcInfo struct {
 	Status         string                     `json:"status,omitempty"`
 	TGID           int32                      `json:"tgid,omitempty"`
 	Times          cpu.TimesStat              `json:"times,omitempty"`
-	UIDs           []int32                    `json:"uids,omitempty"`
+	UIDs           []uint32                   `json:"uids,omitempty"`
 	Username       string                     `json:"username,omitempty"`
 }
 
diff --git a/metrics.go b/metrics.go
index ac185ea..8431cdb 100644
--- a/metrics.go
+++ b/metrics.go
@@ -34,8 +34,8 @@ import (
 	"time"
 
 	"github.com/prometheus/procfs"
-	"github.com/shirou/gopsutil/v3/cpu"
-	"github.com/shirou/gopsutil/v3/load"
+	"github.com/shirou/gopsutil/cpu"
+	"github.com/shirou/gopsutil/load"
 )
 
 //msgp:clearomitted
